var localizedStrings={
    setupMsg:{
        'en-US':'You are being redirected to a censorship-resistant mirror of the website you are trying to visit',
        'ru-RU':'Вы перенаправляетесь на защищенное от цензуры зеркало веб-сайта, который вы пытаетесь посетить.',
        'zh-CN':'您被重定向到您尝试访问的网站的防审查镜像'
    },
    redirecting:{
         'en-us':'Redirecting',
         'ru-RU':'перенаправление',
         'zh-CN':'重定向'
    }

}
