function showConfirmBox() {
    document.getElementById("overlay").hidden = false;
  }
  function closeConfirmBox() {
    document.getElementById("overlay").hidden = true;
  }

function handleLink (doRdrr)
{

   console.log("handleLink:" + doRdrr);
    var baseLink = document.location.href;

    var hashIdx = baseLink.indexOf("#");
   if (hashIdx == -1)
       return;

    baseLink = decodeURIComponent(baseLink.substring(hashIdx+1));
  //  console.log("src:" + baseLink);

    if (!doRdrr)
    {
	if (baseLink.length>1)
        	document.location.href=baseLink;
	return;
    }

    for (i = 0; i < mirrors.sites.length; i++)
    {
     	var mainDomain = mirrors.sites[i].main_domain;

        if (baseLink.indexOf(mainDomain)!=-1)       
        {


//	console.log("found mirror domain: " + mainDomain);

	 for (n = 0; n < mirrors.sites[i].available_alternatives.length;
 n++)
         {
           var altDomain = mirrors.sites[i].available_alternatives[n].url;
	   console.log("checking altdomain: " + altDomain);
           if(altDomain.indexOf('.onion')==-1){
	 	baseLink = baseLink.replace('https://','');
	 	baseLink = baseLink.replace('www.','');
	 	baseLink = baseLink.replace(mainDomain,altDomain);
//		console.log("new: " + baseLink);
           }
         }
	 break;
         
        } 
    }

    if (baseLink.length>1)
    	document.location.href=baseLink;

}

